# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from .views import SignIn, SignUp, LogOut

urlpatterns = patterns('',
    url(r'signin/$', SignIn.as_view(), name='signin'),
    url(r'signup/$', SignUp.as_view(), name='signup'),
    url(r'logout/$', LogOut.as_view(), name= 'logout'),
)