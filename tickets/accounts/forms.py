# -*- coding: utf-8 -*-
from django import forms
from django.contrib import auth
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User

class RegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username','email', 'password1', 'password2')