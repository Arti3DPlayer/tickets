# -*- coding: utf-8 -*-
from django import forms
from django.forms.models import modelformset_factory, inlineformset_factory

from django.contrib.auth.models import User
from .models import Organization, Staff

class OrganizationAddForm(forms.ModelForm):
    class Meta:
        model = Organization
        exclude = ('pub_date', 'active', 'owner')

class StaffAddForm(forms.ModelForm):
    class Meta:
        model = Staff
        exclude = ('organization',)


StaffFormSet = inlineformset_factory(Organization, Staff, form=StaffAddForm, extra=1)