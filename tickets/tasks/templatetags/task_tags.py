# -*- coding: utf-8 -*-
from django.conf import settings
from django import template
from django.utils.translation import ugettext_lazy as _

register = template.Library()

@register.filter
def change_list_header(opts, display_list):
    names = list()
    for name in display_list:
        names.append(opts.get_field_by_name(name)[0].verbose_name)
    return names


@register.filter
def change_list_result(queryset, display_list):
    return queryset.values_list(*display_list)