# -*- coding: utf-8 -*-
from django.shortcuts import redirect, get_object_or_404, render
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib import auth, messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.views.generic.base import View
from django.utils.translation import ugettext_lazy as _
from django.views.generic import ListView, DetailView, FormView, TemplateView, CreateView, UpdateView, DeleteView

from tickets.accounts.utils import LoginRequiredMixin
#from .utils import ViewsGenerate, BaseGenerateView
from .forms import OrganizationAddForm, StaffFormSet
from .models import Ticket, Organization, Job, Staff

#class JobView(LoginRequiredMixin, BaseGenerateView):
#   model = Job
#    display_list = ['id','title']

    #def get_context_data(self, **kwargs):
    #    ctx = super(JobView, self).get_context_data(**kwargs)
    #    print ctx
    #    return ctx

#views_generate = ViewsGenerate('tasks')
#views_generate.register(JobView)
#views_generate.generate()

class JobListView(LoginRequiredMixin,ListView):
    model = Job
    template_name = 'tasks/job_list.html'
    context_object_name = 'jobs'

class JobCreateView(LoginRequiredMixin, CreateView):
    model = Job
    template_name = 'tasks/job_change_form.html'

    def get_success_url(self):
        if self.request.POST.get('save-continue'):
            return reverse_lazy('tasks:job_edit',args=(self.object.id,))
        messages.success(self.request, _(u'Специализация "%s" успешно создана.')%self.object)
        return reverse_lazy('tasks:job_list')

class JobUpdateView(LoginRequiredMixin, UpdateView):
    model = Job
    template_name = 'tasks/job_change_form.html'

    def get_success_url(self):
        if self.request.POST.get('save-continue'):
            return reverse_lazy('tasks:job_edit',args=(self.object.id,))
        messages.success(self.request, _(u'Специализация "%s" изменена.')%self.object)
        return reverse_lazy('tasks:job_list')

class JobDeleteView(LoginRequiredMixin, DeleteView):
    model = Job
    template_name = 'tasks/confirm_delete.html'

    def get_success_url(self):
        messages.success(self.request, _(u'Специализация "%s" удалена.')%self.object)
        return reverse_lazy('tasks:job_list')

class OrganizationListView(LoginRequiredMixin,ListView):
    model = Organization
    template_name = 'tasks/organization_list.html'
    context_object_name = 'organizations'

    def get_query_set(self):
        self.queryset = Organization.objects.filter(owner=self.request.user).order_by('-pub_date')
        return self.queryset

class OrganizationCreateView(LoginRequiredMixin, View):
    form_class = OrganizationAddForm
    form_set = StaffFormSet
    template_name = 'tasks/add_organization.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        formset = self.form_set()
        return render(request, self.template_name, {'form': form, 'formset': formset})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        formset = self.form_set(request.POST, request.FILES, instance=Organization())

        if form.is_valid() and formset.is_valid():
            #cd = form.cleaned_data
            f = form.save(commit=False)
            f.owner = self.request.user
            f.save()
            formset = self.form_set(request.POST, request.FILES, instance=f)
            for form in formset:
                form.save()

            messages.success(request, _(u"Организация %s создана.")%f.title)
            if self.request.POST.get('save-continue'):
                return HttpResponseRedirect(reverse('tasks:organization_edit',args=(f.id,)))
            return HttpResponseRedirect(reverse('tasks:organization_list'))

        return render(request, self.template_name, {'form': form, 'formset': formset})