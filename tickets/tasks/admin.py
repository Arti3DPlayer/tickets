# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import JobPermission, Organization, Staff

class JobPermissionAdmin(admin.ModelAdmin):
    list_display = ('title',)

class StaffInline(admin.TabularInline):
    model = Staff
    extra=1

class OrganizationAdmin(admin.ModelAdmin):
    list_display = ('title',)
    inlines = [StaffInline,]


admin.site.register(JobPermission,JobPermissionAdmin)
admin.site.register(Organization,OrganizationAdmin)
