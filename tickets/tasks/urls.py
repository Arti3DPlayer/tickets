# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from .views import OrganizationListView, OrganizationCreateView, \
                    JobCreateView, JobUpdateView, JobListView, JobDeleteView

urlpatterns = patterns('',
    url(r'^$', OrganizationListView.as_view(), name='organization_list'),
    url(r'^organization/add/$', OrganizationCreateView.as_view(), name='organization_add'),

    url(r'^job/list/$', JobListView.as_view(), name='job_list'),
    url(r'^job/add/$', JobCreateView.as_view(), name='job_add'),
    url(r'^job/edit/(?P<pk>\d+)/$', JobUpdateView.as_view(), name='job_edit'),
    url(r'^job/delete/(?P<pk>\d+)/$', JobDeleteView.as_view(), name='job_delete'),
    #url(r'^ticket/edit/(?P<pk>\d+)/$', TicketEditView.as_view(), name='ticket_edit'),
)