from django.conf.urls import patterns, include, url
from django.core.urlresolvers import reverse, reverse_lazy
from tickets.accounts.utils import LoginRequiredMixin
from django.views.generic.base import View
from django.views.generic import ListView, DetailView, FormView, TemplateView, CreateView, UpdateView

class BaseGenerateView(object):
    display_list = list()

    def get_success_url(self):
        return reverse('%s:%s_edit' % (self.model._meta.app_label, self.model.__name__.lower()),
                args=(self.object.id,))

    def get_context_data(self, **kwargs):
        if 'view' not in kwargs:
            kwargs['view'] = self
        kwargs['opts'] = self.model._meta
        kwargs['display_list'] = self.display_list
        #print self.model.objects.all().values('title')
        return kwargs


class ViewsGenerate(object):
    """
    Class that generates create,update,delete views for model.
    """
    def __init__(self, app):
        self.app = app
        self.base_views = list()
        self.views = dict()
        self.urls = list()
    
    def register(self,base_view):
        self.base_views.append(base_view)
        return self

    def generate(self):
        self.generate_views()
        self.generate_urls()
        return self
        
    def generate_views(self):
        for base_view in self.base_views:
            self.views[base_view] = dict(
                list = self._gen_list_view(base_view),
                create = self._gen_create_view(base_view),
                #edit = self._gen_edit_view(model)
            )

    def _gen_list_view(self, base_view):
        view = type('%sListView' % base_view.model.__name__,
            (base_view, ListView),
            dict(model=base_view.model,
                context_object_name = 'list', 
                template_name='%s/list.html' % self.app
                )
            )

        return view
        
    def _gen_create_view(self, base_view):
        view = type('%sCreateView' % base_view.model.__name__,
            (base_view, CreateView),
            dict(model=base_view.model, template_name='%s/change_form.html' % self.app))

    #    self._set_view_func_get_context_data(view, model)
        return view

    #def _gen_edit_view(self, model):
    #    view = type('%sEditView' % model.__name__,
    #        (LoginRequiredMixin, UpdateView),
    #        dict(model=model, template_name='%s/change_form.html' % self.app))

    #    self._set_view_func_get_success_url(view, model)
    #    self._set_view_func_get_context_data(view, model)      
    #    return view
           

    #def _set_view_func_get_success_url(self, view, model):    
    #    def get_succes_url(self): 
    #        return reverse('%s:%s_edit' % (self.app, model.__name__.lower()),
    #            args=(self.object.id,))
        
    #    view.get_succes_url = get_succes_url

    #def _set_view_func_get_context_data(self, view, model):
    #    def get_context_data(self, **kwargs):
    #        context = super(self.__class__, self).get_context_data(**kwargs)
    #        context['title'] = model._meta.verbose_name
    #        return context
        
    #    view.get_contex_data = get_context_data

    def generate_urls(self):
        for base_view, view in self.views.items():
            self.urls.append(self._gen_list_url(base_view, view['list']))
            self.urls.append(self._gen_create_url(base_view, view['create']))
    #        self.urls.append(self._gen_edit_url(model, views['edit']))

    def _gen_list_url(self, base_view, view):
        return url(r'^%s/list/$' %  base_view.model.__name__.lower(), 
            view.as_view(), name='%s_list' %  base_view.model.__name__.lower())


    def _gen_create_url(self, base_view, view):
        return url(r'^%s/add/$' %  base_view.model.__name__.lower(), 
            view.as_view(), name='%s_add' %  base_view.model.__name__.lower())

    #def _gen_edit_url(self, model, view):
        #return url(r'^%s/edit/$' %  model.__name__.lower(), 
            #view.as_view(), name='%s_edit' %  model.__name__.lower())
    
    def get_urls(self):
        return patterns('', *self.urls)