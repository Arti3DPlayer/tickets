# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.contrib.auth.models import User, Permission

class JobPermission(models.Model):
    title = models.CharField(max_length=30, verbose_name=_(u"Название"))

    class Meta:
        verbose_name = _(u"Права должности")
        verbose_name_plural = _(u"Права должностей")

    def __unicode__(self):
        return self.title

class Job(models.Model):
    title = models.CharField(max_length=30, verbose_name=_(u"Название должности"), help_text=_(u"Например: Менеджер"))
    permission = models.ManyToManyField(JobPermission, verbose_name=_(u"Права"))

    class Meta:
        verbose_name = _(u"Специализация")
        verbose_name_plural = _(u"Специализации")

    def __unicode__(self):
        return self.title

class Organization(models.Model):
    title = models.CharField(max_length=30, verbose_name=_(u"Название"))
    owner = models.ForeignKey(User)
    pub_date = models.DateTimeField(auto_now=True, auto_now_add=True)
    active = models.BooleanField(default=True)

    class Meta:
        verbose_name = _(u"Организация")
        verbose_name_plural = _(u"Организации")

    def __unicode__(self):
        return self.title



class Staff(models.Model):
    user = models.ForeignKey(User, verbose_name=_(u"Юзер"))
    job = models.ForeignKey(Job, verbose_name=_(u"Должность"))
    organization = models.ForeignKey(Organization, verbose_name=_(u"Организация"))

class Ticket(models.Model):
    title = models.CharField(max_length=30, verbose_name="Title")
    pub_date = models.DateTimeField(auto_now=True, auto_now_add=True)
    user = models.ForeignKey(User)

    class Meta:
        verbose_name = "Ticket"


class Task(models.Model):
    title = models.CharField(max_length=200)
    pub_date = models.DateTimeField(auto_now=True, auto_now_add=True)
    ticket = models.ForeignKey('Task')
    user = models.ForeignKey(User)