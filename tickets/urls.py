from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^accounts/', include('tickets.accounts.urls', 'accounts')),
    url(r'', include('tickets.tasks.urls', 'tasks')),
    url(r'^admin/', include(admin.site.urls)),
)
